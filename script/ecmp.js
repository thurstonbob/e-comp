			$.extend( $.fn.dataTable.defaults, {
				searching: false,
				ordering:  false,
				paging: false,
				bInfo : false,
				  "language": {
    "decimal": ",",
    "thousands": "."
  }
			} );
			function setSel(){
				if($('#typ').val() == "free"){
					$("#mois2").prop('disabled', false);
					$("#mois2").selectpicker('refresh');
					$("#annee2").prop('disabled', false);
					$("#annee2").selectpicker('refresh');
				}
				else {
					if($('#typ').val() == "aam1"){
						$("#mois2").val($("#mois1").val())
						$("#annee2").val($("#annee1").val()-1)
						
						}else{
						$("#mois2").val("0")
						$("#annee2").val($("#annee1").val())
					}
					$("#mois2").prop('disabled', true);
					$("#mois2").selectpicker('refresh');
					$("#annee2").prop('disabled', true);
					$("#annee2").selectpicker('refresh');
					}
					
				}
				$(document).ready(function (e) {
					
					
					setSel();
					
					$('select').change(function(e){
						setSel();
					});
					$("#inv").click(function (e){
						e.preventDefault();
						var tmpm1 = $("#mois1").val();
						var tmpa1 = $("#annee1").val();
						$("#mois1").val($("#mois2").val());
						$("#annee1").val($("#annee2").val());
						$("#mois2").val(tmpm1);
						$("#annee2").val(tmpa1);
						$("#mois1").selectpicker('refresh');
						$("#mois2").selectpicker('refresh');
						$("#annee1").selectpicker('refresh');
						$("#annee2").selectpicker('refresh');
						$('#typ').val("free");
						$("#typ").selectpicker('refresh');
					});
					$("form[ajax=true]").submit(function (e) {
						$("#mois2").prop('disabled', false);
						$("#mois2").selectpicker('refresh');
						$("#annee2").prop('disabled', false);
						$("#annee2").selectpicker('refresh');
						
						var $btn = $('#myButton').button('loading');
						e.preventDefault();
						var form_data = $(this).serialize();
						var form_url = $(this).attr("action");
						var form_method = $(this).attr("method").toUpperCase();
						$.ajax({
							url: form_url,
							type: form_method,
							data: form_data,
							cache: false,
							success: function (returnhtml) {
								$("#per").html("Comparaison "+$('#mois :selected').text()+" "+$('#annee').val()+"/"+($('#annee').val()-1));
								$btn.button('reset');
								$('#result').html(returnhtml);
								$('#pres').remove();
								setSel()
							}
						});
					});
				});

