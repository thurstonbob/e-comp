#!/bin/bash
source "script/.proxy_conf"
MOIS1=$1
MOISM1=$(($1-1))
if [ $MOISM1 -eq 0 ]
then
    MOISM1=-1
fi
ANNEE1=$2
MOIS2=$3
ANNEE2=$4
PREF=""
IDETAB=""
#**************GEN*************
rm -f arch/tmp_av*
#IDETAB=`curl -x http://$login:$pwd@$host:$port  -s   --location-trusted -b .cookieJar -c .cookieJar "https://epmsi.atih.sante.fr/appli_16.do?champPmsi=1&statut=1&applicationType=3"  |sed -nr '/hospitalInfo.do\?id/{s/.*>([0-9]+)<.*/\1/g;p}'`
IDETAB=`script/connectEpmsi.sh $5 $6`
#**************CHU**********
CHU1="arch/${PREF}${IDETAB}_${ANNEE1}_${MOIS1}.html"
if [ ! -f $CHU1 ] || [ "$MOIS1" -eq 0 ]
then
   script/connectEpmsi.sh $5 $6>log
   TURLTMP=$(curl -x http://$login:$pwd@$host:$port -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/appli_16.do?champPmsi=1&statut=1&applicationType=3" "https://epmsi.atih.sante.fr/appli_05.do?year=$ANNEE1&period=$MOIS1")
    TURL=$(grep "appli_05.zip" <(echo "$TURLTMP")|sed -re 's/^.*href="(.*)".*$/\1/;s/&amp;/\&/g')
    if ! grep --quiet  "Cet exercice est validé" <(echo "$TURLTMP") ; then
        PREF="tmp_"
        CHU1="arch/${PREF}${IDETAB}_${ANNEE1}_${MOIS1}.html"
    fi

    if [ -z "$TURL" ]; then
        echo $login
        echo $CHU1
        echo $IDETAB
        echo $TURLTMP> logt.html
        echo "pb connexion ePMSI période 1"
        exit 1
    fi
    curl -x http://$login:$pwd@$host:$port    -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/appli_05.do?year=$ANNEE1&period=$MOIS1" "https://epmsi.atih.sante.fr$TURL" --output output.zip
    #est ce que la période est validée ? si non il fait un nom temporaire 
    rm -f OVALIDE*.html
    unzip -q output.zip
    mv  OVALIDE*.html $CHU1
    recode ISO-8859-1..UTF8 $CHU1 
    rm output.zip
fi
#est ce que la période est validée ? si non il faut recharger le fichier
#Arrete de versement
AV1="arch/${PREF}av_${IDETAB}_${ANNEE1}_${MOIS1}.html"
if [ ! -f $AV1 ] || [ "$MOIS1" -eq 0 ]
then
    script/connectEpmsi.sh $5 $6 >log
    TURLTMP=$(curl -x http://$login:$pwd@$host:$port    -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/appli_16.do?champPmsi=1&statut=1&applicationType=3" "https://epmsi.atih.sante.fr/appli_08.do?year=$ANNEE1&period=$MOIS1")

    if ! grep --quiet  "Cet exercice est validé" <(echo "$TURLTMP") ; then
        PREF="tmp_"
        AV1="arch/${PREF}av_${IDETAB}_${ANNEE1}_${MOIS1}.html"
    fi
    curl -x http://$login:$pwd@$host:$port    -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/appli_08.do?year=$ANNEE1&period=$MOIS1" "https://epmsi.atih.sante.fr/appli_08.xls?command=1" --output $AV1
fi

if [ $MOISM1 -gt 0 ]
then
    AVM1="arch/${PREF}av_${IDETAB}_${ANNEE1}_${MOISM1}.html"
    if [ ! -f $AVM1 ]
    then
    script/connectEpmsi.sh $5 $6 >log
        TURLTMP=$(curl -x http://$login:$pwd@$host:$port  -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/appli_16.do?champPmsi=1&statut=1&applicationType=3" "https://epmsi.atih.sante.fr/appli_08.do?year=$ANNEE1&period=$MOISM1")

        if ! grep --quiet  "Cet exercice est validé" <(echo "$TURLTMP") ; then
            PREF="tmp_"
            AVM1="arch/${PREF}av_${IDETAB}_${ANNEE1}_${MOISM1}.html"
        fi
        curl -x http://$login:$pwd@$host:$port -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/appli_08.do?year=$ANNEE1&period=$MOISM1" "https://epmsi.atih.sante.fr/appli_08.xls?command=1" --output $AVM1


    fi
fi

T1="tmp/t1.txt"
rm -f $T1 
sed '/EDMS\]/,/Separateur de Tableau/!d;/Separateur de Tableau/q' $CHU1|grep -E -A1  --no-group-separator "l t data"|sed -re 's/<td.*>(.*)<\/td>/\1/g'|paste - - -d';' | gawk -F';' '!/Perf/{gsub(" ","",$2);sub(",",".",$    2);arr[$1]+=$2}END {for (i in arr) {print i";"sprintf("%.2f",arr[i])}}'>$T1


CHU2="arch/${IDETAB}_${ANNEE2}_${MOIS2}.html"
if [ ! -f $CHU2 ] || [ "$MOIS2" -eq 0 ]
then
    script/connectEpmsi.sh $5 $6 >log
    TURLTMP=$(curl -x http://$login:$pwd@$host:$port  --proxy-basic  -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/appli_16.do?champPmsi=1&statut=1&applicationType=3" "https://epmsi.atih.sante.fr/appli_05.do?year=$ANNEE2&period=$MOIS2")
    TURL=$(grep "appli_05.zip" <(echo "$TURLTMP")|sed -re 's/^.*href="(.*)".*$/\1/;s/&amp;/\&/g')
    if ! grep --quiet  "Cet exercice est validé" <(echo "$TURLTMP") ; then
        PREF="tmp_"
        CHU2="arch/${PREF}${IDETAB}_${ANNEE2}_${MOIS2}.html"
    fi

    if [ -z "$TURL" ]; then
        echo "pb connexion ePMSI periode 2"
        exit 1
    fi
    curl -x http://$login:$pwd@$host:$port  --proxy-basic  -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/appli_05.do?year=$ANNEE2&period=$MOIS2" "https://epmsi.atih.sante.fr$TURL" --output output.zip
    #est ce que la période est validée ? si non il fait un nom temporaire 
    rm -f OVALIDE*.html
    unzip -q output.zip
    mv  OVALIDE*.html $CHU2
    recode ISO-8859-1..UTF8 $CHU2 
    rm output.zip
fi

T2="tmp/t2.txt"
rm -f $T2 
sed '/EDMS\]/,/Separateur de Tableau/!d;/Separateur de Tableau/q' $CHU2|grep -E -A1  --no-group-separator "l t data"|sed -re 's/<td.*>(.*)<\/td>/\1/g'|paste - - -d';' | gawk -F';' '!/Perf/{gsub(" ","",$2);sub(",",".",$    2);arr[$1]+=$2}END {for (i in arr) {print i";"sprintf("%.2f",arr[i])}}'>$T2

rm -f res/gen_ipdms.txt
awk -F';' '
function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
function trim(s) { return rtrim(ltrim(s)); }
FILENAME~/T?1/{
if(trim($1) ~ /standardisées/){
    valo_s+=$2;
}
       else
           {
               valo_e+=$2;
           }
           next;
       }
       FILENAME~/T?2/{
       if(trim($1) ~ /standardisées/){
           valo2_s+=$2;
       }
   else
       {
           valo2_e+=$2;
       }
       next;
   }

   END{
   ipdms = valo_e/valo_s;
   ipdms2 = valo2_e/valo2_s;
   tx=100*(ipdms-ipdms2)/ipdms2;

   if(valo2_e!=0){
       pc_e=100*(valo_e-valo2_e)/valo2_e
   }else
   {
       pc_e=""
   }
   if(valo2_s!=0){
       pc_s=100*(valo_s-valo2_s)/valo2_s
   }else
   {
       pc_s="\"\""
   }

   if(valo_e-valo2_e<0){
       sup1="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>";
   }
       else if (valo_e-valo2_e>0){
           sup1="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
       }
   else{
       sup1="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
   }
   if(valo_s-valo2_s<0){
       sup2="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
   }
else if (valo_s-valo2_s>0){
    sup2="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
}
else{
    sup2="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
if(tx<0){sup3="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"}
else if (tx>0){sup3="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"}
else{sup3="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
    print "{\"data\": [";

    print "[\"Nombre  de journ&eacute;es &Eacute;tablissement\","valo_e","valo2_e","valo_e - valo2_e","pc_e",\""sup1"\"],";
    print "[\"Nombre  de journ&eacute;es standardis&eacute;es\","valo_s","valo2_s","valo_s - valo2_s","pc_s",\""sup2"\"],";
    print "[\"IPDMS\","ipdms","ipdms2","ipdms-ipdms2","tx",\""sup3"\"]";
    print "]}"
}

' $T1  "$T2" > res/gen_ipdms.txt
#**************GEN2 MCO**********************
rm -f res/gen_rsa.txt

sed  '/\[1\.D\.2\.RTP\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 |grep -E -A1  --no-group-separator "l t data"|sed -e 's/<td .*>\(.*\)<\/td>/\1/g'|paste - - -d'|'|recode UTF8..html>$T1

sed  '/\[1\.D\.2\.RTP\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 |grep -E -A1  --no-group-separator "l t data"|sed -e 's/<td .*>\(.*\)<\/td>/\1/g'|paste - - -d'|'|recode UTF8..html>$T2


awk -F'|' '
function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
function trim(s) { return rtrim(ltrim(s)); }
FILENAME~/T?1/{
gsub(" ","",$2);
gsub(",",".",$2);
if(trim($1) in valo == 0)order[i++]=trim($1);
    valo[trim($1)]+=$2;
    next;
}
FILENAME~/T?2/{
gsub(" ","",$2);
gsub(",",".",$2);
if(trim($1)in valo){
    valo2[trim($1)]+=$2;
}
next;
    }

END{
print "{\"data\": [";


for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=sprintf("%-\478d",s_1-s_2);
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
            else{
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"</span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}

        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/gen_rsa.txt

        #****************************HOSPIT MCO****************************
        sed -rn -e '/\[1\.V\.1\.RAV\]/,/Separateur de Tableau/{/>Valorisation/{p;n;n;n;p;n;p;n;p}}' $CHU1|sed -r  "s/<td.*>(.*)<\/td>/\1/;s/&#39\;/'/g"|paste - - - - -d'|'|recode UTF8..html>$T1

        sed -rn -e '/\[1\.V\.1\.RAV\]/,/Separateur de Tableau/{/>Valorisation/{p;n;n;n;p;n;p;n;p}}' $CHU2|sed -r  "s/<td.*>(.*)<\/td>/\1/;s/&#39\;/'/g"|paste - - - - -d'|'|recode UTF8..html>$T2



        rm -f  res/hosp_t2a.txt
        rm -f  res/hosp_cp.txt
        rm -f  res/hosp_am.txt
        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/&&trim($1)!~/Coefficient géographique|Coefficient prudentiel.*/{
        gsub(" ","",$2);
        gsub(" ","",$3);
        gsub(" ","",$4);
        gsub(",",".",$2);
        gsub(",",".",$3);
        gsub(",",".",$4);
        if(trim($1) in valo_t2a == 0)order[i++]=trim($1);
            valo_t2a[trim($1)]+=$2;
            valo_cp[trim($1)]+=$3;
            valo_am[trim($1)]+=$4;
            next;
        }
        FILENAME~/T?2/&&trim($1)!~/Coefficient géographique|Coefficient prudentiel.*/{
        gsub(" ","",$2);
        gsub(" ","",$3);
        gsub(" ","",$4);
        gsub(",",".",$2);
        gsub(",",".",$3);
        gsub(",",".",$4);

        if(trim($1)in valo_t2a){
            valo_t2a2[trim($1)]+=$2;
            valo_cp2[trim($1)]+=$3;
            valo_am2[trim($1)]+=$4;
        }
        next;
    }

END{
print "{\"data\": ["> "res/hosp_t2a.txt";
for (j=0;j<i;j++){
    s_t2a_1=valo_t2a[order[j]];
    s_t2a_1g+=s_t2a_1;
    if(order[j] in valo_t2a2){
        s_t2a_2=valo_t2a2[order[j]];
        s_t2a_2g+=s_t2a_2;
        s_t2a_3=s_t2a_1-s_t2a_2;
        s_t2a_3g+=(s_t2a_1-s_t2a_2);
        not_rel=0;
    }
else{
    s_t2a_2="\"\"";s_t2a_3="\"\""
    not_rel=1
}
if(s_t2a_2!=0&&!not_rel) {
    pc_t2a=100*(s_t2a_1-s_t2a_2)/sqrt(s_t2a_2^2)
}
        else{
            pc_t2a="\"\""
        }
        if(s_t2a_1-s_t2a_2<0){
            sup=not_rel?"":"<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            pc_t2a=not_rel?"\"\"":pc_t2a;
        }
    else if (s_t2a_1-s_t2a_2>0){
        sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
    }
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
if(trim(length(order[j]))>0)print "[\""order[j]"\","sprintf("%d",valo_t2a[order[j]])","sprintf("%d",s_t2a_2)","sprintf("%d",s_t2a_3)","pc_t2a",\""sup"\"],"> "res/hosp_t2a.txt";
}
if(s_t2a_2g!=0) {
    pc_t2ag=100*(s_t2a_1g-s_t2a_2g)/s_t2a_2g
}
else{
    pc_t2ag="\"\""
}

if(s_t2a_3g<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
else if (s_t2a_3g>0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
}
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
print "[\"Total\","sprintf("%d",s_t2a_1g)","sprintf("%d",s_t2a_2g)","sprintf("%d",s_t2a_3g)","pc_t2ag",\""sup"\"]]}"> "res/hosp_t2a.txt";
print "{\"data\": ["> "res/hosp_cp.txt";
for (j=0;j<i;j++){
    s_cp_1=valo_cp[order[j]];
    s_cp_1g+=s_cp_1;
    if(order[j] in valo_t2a2){
        s_cp_2=valo_cp2[order[j]];
        s_cp_2g+=s_cp_2;
        s_cp_3=s_cp_1-s_cp_2;
        s_cp_3g+=(s_cp_1-s_cp_2);
        not_rel=0;
    }
else{
    s_cp_2="\"\"";s_cp_3="\"\"";
    not_rel=1;
}
if(s_cp_2!=0&&!not_rel) {
    pc_cp=100*(s_cp_1-s_cp_2)/sqrt(s_cp_2^2)
}
    else{
        pc_cp="\"\""
    }
    if(s_cp_1-s_cp_2<0){
        sup=not_rel?"":"<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
        pc_cp=not_rel?"\"\"":pc_cp
    }
else if (s_cp_1-s_cp_2>0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
}
    else{
        sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
    }
    if(trim(length(order[j]))>0)print "[\""order[j]"\","sprintf("%d",valo_cp[order[j]])","sprintf("%d",s_cp_2)","sprintf("%d",s_cp_3)","pc_cp",\""sup"\"],"> "res/hosp_cp.txt";
    }
    if(s_cp_2g!=0) {
        pc_cpg=100*(s_cp_1g-s_cp_2g)/s_cp_2g
    }
else{
    pc_cpg="\"\""
}
if(s_cp_3g<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
else if (s_cp_3g>0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
}
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
print "[\"Total\","sprintf("%d",s_cp_1g)","sprintf("%d",s_cp_2g)","sprintf("%d",s_cp_3g)","pc_cpg",\""sup"\"]]}"> "res/hosp_cp.txt";
print "{\"data\": ["> "res/hosp_am.txt";
for (j=0;j<i;j++){
    s_am_1=valo_am[order[j]];
    s_am_1g+=s_am_1;
    if(order[j] in valo_t2a2){
        s_am_2=valo_am2[order[j]];
        s_am_2g+=s_am_2;
        s_am_3=s_am_1-s_am_2;
        s_am_3g+=(s_am_1-s_am_2);
        not_rel=0;
    }
else{
    s_am_2="\"\"";s_am_3="\"\"";
    not_rel=1;
}
if(s_am_2!=0&&!not_rel) {
    pc_am=100*(s_am_1-s_am_2)/sqrt(s_am_2^2)
}
        else{
            pc_am="\"\""
        }
        if(s_am_1-s_am_2<0){
            sup=not_rel?"":"<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            pc_am=not_rel?"\"\"":pc_am
        }
    else if (s_am_1-s_am_2>0){
        sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
    }
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
if(trim(length(order[j]))>0)print "[\""order[j]"\","sprintf("%d",valo_am[order[j]])","sprintf("%d",s_am_2)","sprintf("%d",s_am_3)","pc_am",\""sup"\"],"> "res/hosp_am.txt";
}
if(s_am_2g!=0) {
    pc_amg=100*(s_am_1g-s_am_2g)/s_am_2g
}
else{
    pc_amg="\"\""
}
if(s_am_3g<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
else if (s_am_3g>0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
}
    else{
        sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
    }
    print "[\"Total\","sprintf("%d",s_am_1g)","sprintf("%d",s_am_2g)","sprintf("%d",s_am_3g)","pc_amg",\""sup"\"]]}"> "res/hosp_am.txt";
}' $T1  $T2
#****************************EXTERNE****************************

sed  '/\[2\.V\.RAEM\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1|grep -E -A4  --no-group-separator "l t data"|sed -e 's/<td .*>\(.*\)<\/td>/\1/g'|paste - - - - - -d'|'>$T1
sed  '/\[2\.V\.RAEM\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2|grep -E -A4  --no-group-separator "l t data"|sed -e 's/<td .*>\(.*\)<\/td>/\1/g'|paste - - - - - -d'|'>$T2


rm -f  res/ext_t2a.txt
rm -f  res/ext_am.txt
rm -f  res/ext_t2a_u.txt
rm -f  res/ext_am_u.txt
awk -F'|' '
function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
function trim(s) { return rtrim(ltrim(s)); }
FILENAME~/T?1/{
gsub(" ","",$2);
gsub(" ","",$3);
gsub(" ","",$4);
gsub(" ","",$5);
gsub(",",".",$2);
gsub(",",".",$3);
gsub(",",".",$4);
gsub(",",".",$5);
if(trim($1) in valo_t2a == 0)order[i++]=trim($1);
    valo_t2a[trim($1)]+=$2;
    valo_am[trim($1)]+=$3;
    valo_t2a_u[trim($1)]+=$4;
    valo_am_u[trim($1)]+=$5;

    next;
}
FILENAME~/T?2/{
gsub(" ","",$2);
gsub(" ","",$3);
gsub(" ","",$4);
gsub(" ","",$5);
gsub(",",".",$2);
gsub(",",".",$3);
gsub(",",".",$4);
gsub(",",".",$5);

if(trim($1)in valo_t2a){
    valo_t2a2[trim($1)]+=$2;
    valo_am2[trim($1)]+=$3;
    valo_t2a_u2[trim($1)]+=$4;
    valo_am_u2[trim($1)]+=$5;

}
next;
                }

            END{
            print "{\"data\": ["> "res/ext_t2a.txt";
            for (j=0;j<i;j++){
                s_t2a_1=valo_t2a[order[j]];
                s_t2a_1g+=s_t2a_1;
                if(order[j] in valo_t2a2){
                    s_t2a_2=valo_t2a2[order[j]];
                    s_t2a_2g+=s_t2a_2;
                    s_t2a_3=s_t2a_1-s_t2a_2;
                    s_t2a_3g+=(s_t2a_1-s_t2a_2);
                }
            else{
                s_t2a_2="\"\"";s_t2a_3="\"\""
                not_rel=1

            }
            if(s_t2a_2!=0&&!not_rel) {
                pc_t2a=100*(s_t2a_1-s_t2a_2)/sqrt(s_t2a_2^2)
            }
        else{
            pc_t2a="\"\""
        }
        if(s_t2a_1-s_t2a_2<0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
        }
    else if(s_t2a_1-s_t2a_2>0){
        sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
    }
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
if(trim(length(order[j]))>0)print "[\""order[j]"\","sprintf("%d",valo_t2a[order[j]])","sprintf("%d",s_t2a_2)","sprintf("%d",s_t2a_3)","pc_t2a",\""sup"\"],"> "res/ext_t2a.txt";
}
if(s_t2a_2g!=0) {
    pc_t2ag=100*(s_t2a_1g-s_t2a_2g)/s_t2a_2g
}
        else{
            pc_t2ag="\"\""
        }

        if(s_t2a_3g<0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
        }
    else if (s_t2a_3g>0){
        sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
    }
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
print "[\"Total\","sprintf("%d",s_t2a_1g)","sprintf("%d",s_t2a_2g)","sprintf("%d",s_t2a_3g)","pc_t2ag",\""sup"\"]]}"> "res/ext_t2a.txt";
print "{\"data\": ["> "res/ext_am.txt";
for (j=0;j<i;j++){
    s_am_1=valo_am[order[j]];
    s_am_1g+=s_am_1;
    if(order[j] in valo_t2a2){
        s_am_2=valo_am2[order[j]];
        s_am_2g+=s_am_2;
        s_am_3=s_am_1-s_am_2;
        s_am_3g+=(s_am_1-s_am_2);
        not_rel=0
    }
else{
    s_am_2="\"\"";s_am_3="\"\"";
    not_rel=1
}
if(s_am_2!=0&&!not_rel) {
    pc_am=100*(s_am_1-s_am_2)/sqrt(s_am_2^2)
}
    else{
        pc_am="\"\""
    }
    if(s_am_1-s_am_2<0){
        sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
    }
else if (s_am_1-s_am_2>0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
}
            else{
                sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
            }
            if(trim(length(order[j]))>0)print "[\""order[j]"\","sprintf("%d",valo_am[order[j]])","sprintf("%d",s_am_2)","sprintf("%d",s_am_3)","pc_am",\""sup"\"],"> "res/ext_am.txt";
            }
            if(s_am_3g<0)col="danger";
                if(s_am_2g!=0) {
                    pc_amg=100*(s_am_1g-s_am_2g)/s_am_2g
                }
            else{
                pc_amg="\"\""
            }
            if(s_am_3g<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_am_3g>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{
        sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
    }
    print "[\"Total\","sprintf("%d",s_am_1g)","sprintf("%d",s_am_2g)","sprintf("%d",s_am_3g)","pc_amg",\""sup"\"]]}"> "res/ext_am.txt";
    print "{\"data\": ["> "res/ext_t2a_u.txt";
    for (j=0;j<i;j++){
        s_t2a_u1=valo_t2a_u[order[j]];
        s_t2a_u1g+=s_t2a_u1;
        if(order[j] in valo_t2a2){
            s_t2a_u2=valo_t2a_u2[order[j]];
            s_t2a_u2g+=s_t2a_u2;
            s_t2a_u3=s_t2a_u1-s_t2a_u2;
            s_t2a_u3g+=(s_t2a_u1-s_t2a_u2);
        not_rel=0
        }
    else{
        s_t2a_u2="\"\"";s_t2a_u3="\"\""
        not_rel=1
    }
    if(s_t2a_u2!=0&&!not_rel) {
        pc_t2a_u=100*(s_t2a_u1-s_t2a_u2)/sqrt(s_t2a_u2^2)
    }
else{
    pc_t2a_u="\"\""
}
if(s_t2a_u1-s_t2a_u2<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
else if (s_t2a_u1-s_t2a_u2>0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
}
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
if(trim(length(order[j]))>0)print "[\""order[j]"\","sprintf("%d",valo_t2a_u[order[j]])","sprintf("%d",s_t2a_u2)","sprintf("%d",s_t2a_u3)","pc_t2a_u",\""sup"\"],"> "res/ext_t2a_u.txt";
}
if(s_t2a_u2g!=0) {
    pc_t2a_ug=100*(s_t2a_u1g-s_t2a_u2g)/s_t2a_u2g
}
                    else{
                        pc_t2a_ug="\"\""
                    }
                    if(s_t2a_u3g<0){
                        sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
                    }
                else if (s_t2a_u3g>0){
                    sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
                }
            else{
                sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
            }
            print "[\"Total\","sprintf("%d",s_t2a_u1g)","sprintf("%d",s_t2a_u2g)","sprintf("%d",s_t2a_u3g)","pc_t2a_ug",\""sup"\"]]}"> "res/ext_t2a_u.txt";
            print "{\"data\": ["> "res/ext_am_u.txt";
            for (j=0;j<i;j++){
                s_am_u1=valo_am_u[order[j]];
                s_am_u1g+=s_am_u1;
                if(order[j] in valo_t2a2){
                    s_am_u2=valo_am_u2[order[j]];
                    s_am_u2g+=s_am_u2;
                    s_am_u3=s_am_u1-s_am_u2;
                    s_am_u3g+=(s_am_u1-s_am_u2);
        not_rel=0
                }
            else{
                s_am_u2="\"\"";s_am_u3="\"\"";
                not_rel=1
            }
            if(s_am_u2!=0&&!not_rel) {
                pc_am_u=100*(s_am_u1-s_am_u2)/sqrt(s_am_u2^2)
            }
        else{
            pc_am_u="\"\""
        }
        if(s_am_u1-s_am_u2<0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
        }
    else if (s_am_u1-s_am_u2>0){
        sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
    }
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
if(trim(length(order[j]))>0)print "[\""order[j]"\","sprintf("%d",valo_am_u[order[j]])","sprintf("%d",s_am_u2)","sprintf("%d",s_am_u3)","pc_am_u",\""sup"\"],"> "res/ext_am_u.txt";
}
if(s_am_u2g!=0) {
    pc_am_ug=100*(s_am_u1g-s_am_u2g)/s_am_u2g
}
else{
    pc_am_ug="\"\""
}
if(s_am_u3g<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
else if (s_am_u3g>0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
}
else{
    sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"
}
print "[\"Total\","sprintf("%d",s_am_u1g)","sprintf("%d",s_am_u2g)","sprintf("%d",s_am_u3g)","pc_am_ug",\""sup"\"]]}"> "res/ext_am_u.txt";
    }


    ' $T1  $T2
    #******************************EXTREMEHB**********************


    sed '/\[1\.V\.1\.RAE\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 |grep -E -A1 --no-group-separator "extr"|sed -e 's/<td .*>\(.*\)<\/td>/\1/g'|paste - - -d'|'|recode UTF8..html>$T1
    sed '/\[1\.V\.1\.RAE\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 |grep -E -A1 --no-group-separator "extr"|sed -e 's/<td .*>\(.*\)<\/td>/\1/g'|paste - - -d'|'|recode UTF8..html >$T2

    rm -f  res/exhb.txt


    awk -F'|' '
    function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
    function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
    function trim(s) { return rtrim(ltrim(s)); }
    FILENAME~/T?1/{
    gsub(" ","",$2);
    gsub(",",".",$2);
    if(trim($1) in valo == 0)order[i++]=trim($1);
        valo[trim($1)]+=$2;
        next;
    }
    FILENAME~/T?2/{
    gsub(" ","",$2);
    gsub(",",".",$2);
    if(trim($1)in valo){
        valo2[trim($1)]+=$2;
    }
    next;
}

    END{
    print "{\"data\": [";
    for (j=0;j<i;j++){
        s_1=valo[order[j]];
        if(order[j] in valo2){
            s_2=valo2[order[j]];
            s_3=s_1-s_2;
        }
    else{
        s_2="\"\"";s_3="\"\""
        not_rel=1
    }
    if(s_2!=0&&!not_rel) {
        pc=100*(s_1-s_2)/s_2
    }
else{
    pc="\"\""
}
if(s_1-s_2<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/exhb.txt

        #******************************Forfait technique**********************
        sed  '/\[2\.V\.VFTN\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed '1d;s/\t//g;s/    /|/'|recode UTF8..html>$T1
        sed  '/\[2\.V\.VFTN\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed '1d;s/\t//g;s/    /|/'|recode UTF8..html>$T2
        rm -f  res/ft.txt


        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        FILENAME~/T?2/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1)in valo){
            valo2[trim($1)]+=$2;
        }
        next;
    }

END{
print "{\"data\": [";
for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=s_1-s_2;
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
            else{
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/ft.txt




        #******************************IVG**********************


        sed  '/\[1\.V\.1\.VIVG\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed 's/\t//g;s/    /|/'|recode UTF8..html>$T1
        sed  '/\[1\.V\.1\.VIVG\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed 's/\t//g;s/    /|/'|recode UTF8..html>$T2


        rm -f  res/ivg.txt


        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        FILENAME~/T?2/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1)in valo){
            valo2[trim($1)]+=$2;
        }
        next;
    }

END{
print "{\"data\": [";
for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=s_1-s_2;
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
            else{
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/ivg.txt



        #******************************DMI**********************


        sed  '/\[1\.V\..\.VDMI\] B/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed -r 's/\t//g;s/    /|/;s/valorisé [0-9]+/valorisé/'|recode UTF8..html>$T1
        sed  '/\[1\.V\..\.VDMI\] B/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed -r 's/\t//g;s/    /|/;s/valorisé [0-9]+/valorisé/'|recode UTF8..html>$T2

        rm -f  res/dmi.txt


        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        FILENAME~/T?2/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1)in valo){
            valo2[trim($1)]+=$2;
        }
        next;
    }

END{
print "{\"data\": [";
for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=s_1-s_2;
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
            else{
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/dmi.txt




        #******************************MO**********************


        sed  '/\[1\.V\.2\.VMED\] B/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g;s/  / /g;s/responsabilité/resp./' |pr -2 -t|sed -r 's/\t//g;s/    /|/;s/valorisé [0-9]+/valorisé/'|recode UTF8..html>$T1
        sed  '/\[1\.V\.2\.VMED\] B/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g;s/  / /g;s/responsabilité/resp./' |pr -2 -t|sed -r 's/\t//g;s/    /|/;s/valorisé [0-9]+/valorisé/'|recode UTF8..html>$T2

        rm -f  res/mo.txt


        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        FILENAME~/T?2/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1)in valo){
            valo2[trim($1)]+=$2;
        }
        next;
    }

END{
print "{\"data\": [";
for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=s_1-s_2;
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
            else{
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/mo.txt





        #******************************DM ext**********************


        sed  '/\[2\.V\.VDME\] B/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed 's/\t//g;s/    /|/'|recode UTF8..html>$T1
        sed  '/\[2\.V\.VDME\] B/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed 's/\t//g;s/    /|/'|recode UTF8..html>$T2


        rm -f  res/dmext.txt


        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        FILENAME~/T?2/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1)in valo){
            valo2[trim($1)]+=$2;
        }
        next;
    }

END{
print "{\"data\": [";
for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=s_1-s_2;
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
            else{
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/dmext.txt


        rm -f  res/exh.html
        EXH1=$(mysql -BN -ucompuser -pmgy89 -e "select 100*(1-nb_rss_manq/nb_rss_tot) as exh from exhaustivite where annee=$ANNEE1 and mois=$MOIS1" pmsi_dim)
        EXH2=$(mysql -BN -ucompuser -pmgy89 -e "select 100*(1-nb_rss_manq/nb_rss_tot) as exh from exhaustivite where annee=$ANNEE2 and mois=$MOIS2" pmsi_dim)
        if [ -n "$EXH1" ] && [ -n "$EXH2" ]
        then
            if (( $(echo "$EXH1 > $EXH2" |bc -l) ))
            then
                GL="glyphicon glyphicon-arrow-up"
            elif (( $(echo "$EXH1 < $EXH2" |bc -l) ))
            then
                GL="glyphicon glyphicon-arrow-down"
            else

                GL="glyphicon glyphicon-arrow-right"
            fi
        else
            GL="glyphicons glyphicons-question-sign"
            EXH1="${EXH1:-Non disponible}"
            EXH2="${EXH2:-Non disponible}"
        fi
        echo "<h3>Exhaustivit&eacute;</h3><table id=\"exhaust_tab\" class=\"table table-striped table-hover table-condensed\"><tbody><tr><th></th><th style=\"text-align:right\">"$MOIS1 / $ANNEE1"</th><th style=\"text-align:right\">"$MOIS2 / $ANNEE2"</th><th style=\"text-align:right\"></th></tr>
        <tr><th>Taux d'exhaustivit&eacute;</th><td align=\"right\">"$EXH1"</td><td align=\"right\">"$EXH2"</td><td align=\"right\"><span class=\"glyphicon "$GL"\"></span></td></tr></tbody></table><hr>">>res/exh.html

        #******************************AHCBHN**********************

        sed  '/\[5\.D\.AHCBHN\] A/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed -E 's/\t//g;s/(    )|(  )/|/'|recode UTF8..html>$T1
        sed  '/\[5\.D\.AHCBHN\] A/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed -E 's/\t//g;s/(    )|(  )/|/'|recode UTF8..html>$T2

        rm -f  res/ahcbhn.txt


        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        FILENAME~/T?2/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1)in valo){
            valo2[trim($1)]+=$2;
        }
        next;
    }

END{
print "{\"data\": [";
for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=s_1-s_2;
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
                else{
                    pc="\"\""
                }
                if(s_1-s_2<0){
                    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
                }
            else if (s_1-s_2>0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
            }
        else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
            if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
                if(j<i-1)print","
                }
                print "]}"
            }' $T1 $T2> res/ahcbhn.txt

            #******************************AHCBHN2**********************

            sed  '/\[5\.D\.AHCBHN\] B/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed -E 's/\t//g;s/(    )|(  )/|/'|recode UTF8..html>$T1
            sed  '/\[5\.D\.AHCBHN\] B/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g' |pr -2 -t|sed -E 's/\t//g;s/(    )|(  )/|/'|recode UTF8..html>$T2

            rm -f  res/ahcbhn2.txt


            awk -F'|' '
            function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
            function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
            function trim(s) { return rtrim(ltrim(s)); }
            FILENAME~/T?1/{
            gsub(" ","",$2);
            gsub(",",".",$2);
            if(trim($1) in valo == 0)order[i++]=trim($1);
                valo[trim($1)]+=$2;
                next;
            }
            FILENAME~/T?2/{
            gsub(" ","",$2);
            gsub(",",".",$2);
            if(trim($1)in valo){
                valo2[trim($1)]+=$2;
            }
            next;
        }

    END{
    print "{\"data\": [";
    for (j=0;j<i;j++){
        s_1=valo[order[j]];
        if(order[j] in valo2){
            s_2=valo2[order[j]];
            s_3=s_1-s_2;
        }
    else{
        s_2="\"\"";s_3="\"\""
        not_rel=1
    }
    if(s_2!=0&&!not_rel) {
        pc=100*(s_1-s_2)/s_2
    }
else{
    pc="\"\""
}
if(s_1-s_2<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
    else if (s_1-s_2>0){
        sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
    }
else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
    if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
        if(j<i-1)print","
        }
        print "]}"
    }' $T1 $T2> res/ahcbhn2.txt


    #******************************AHCBHN3**********************


    rm -f  res/ahcbhn3.txt

    awk -F',' '
    FNR==NR{
    a[$1]=$2;
    b[$1]=$3;
    next;
}
NF==6{
s_1=a[$1]+$2;
s_2=b[$1]+$3;
if(s_2!=0) {
    pc=100*(s_1-s_2)/s_2
}
        else
            {
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}

        print $1","s_1","s_2","s_1-s_2","pc",\""sup"\"]";
        next;
    }
    {
        print;
    }' res/ahcbhn.txt res/ahcbhn2.txt>res/ahcbhn3.txt
    #******************************SMUR**********************

    sed  '/\[8\.D\.SMUR\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g;s/TIIH.*$/TIIH/' |pr -2 -t|sed -E 's/\t//g;s/(    )|(  )/|/'|recode UTF8..html>$T1
    sed  '/\[8\.D\.SMUR\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g;s/TIIH.*$/TIIH/' |pr -2 -t|sed -E 's/\t//g;s/(    )|(  )/|/'|recode UTF8..html>$T2

    rm -f  res/smur.txt


    awk -F'|' '
    function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
    function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
    function trim(s) { return rtrim(ltrim(s)); }
    FILENAME~/T?1/{
    gsub(" ","",$2);
    gsub(",",".",$2);
    if(trim($1) in valo == 0)order[i++]=trim($1);
        valo[trim($1)]+=$2;
        next;
    }
    FILENAME~/T?2/{
    gsub(" ","",$2);
    gsub(",",".",$2);
    if(trim($1)in valo){
        valo2[trim($1)]+=$2;
    }
    next;
}

    END{
    print "{\"data\": [";
    for (j=0;j<i;j++){
        s_1=valo[order[j]];
        if(order[j] in valo2){
            s_2=valo2[order[j]];
            s_3=s_1-s_2;
        }
    else{
        s_2="\"\"";s_3="\"\""
        not_rel=1
    }
    if(s_2!=0&&!not_rel) {
        pc=100*(s_1-s_2)/s_2
    }
else{
    pc="\"\""
}
if(s_1-s_2<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/smur.txt


        #******************************CES**********************
        sed  '/\[7\.D\.CES\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g'|pr -2 -t -J|sed -E 's/\t/|/'|egrep "(Nb total consult)|(File active)">$T1
        sed  '/\[7\.D\.CES\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | grep "r m header"|sed -E 's/(<br>)|(<br\/>)/ /g;s/<th .*>(.*)<\/th>/\1/g'|pr -2 -t -J|sed -E 's/\t/|/'|egrep "(Nb total consult)|(File active)">$T2

        rm -f  res/ces.txt


        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        FILENAME~/T?2/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1)in valo){
            valo2[trim($1)]+=$2;
        }
        next;
    }

END{
print "{\"data\": [";
for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=s_1-s_2;
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
            else{
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/ces.txt


        #******************************PPCO**********************

        sed  '/\[9\.D\.PPCO\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU1 | egrep 'td class="(l|r) t data"'|sed -E 's/<td .*>(.*)<\/td>/\1/g'|paste - - -d'\|'|egrep "(Nombre total de consultations méd.*primo)|(File active)"|recode UTF8..html>$T1
        sed  '/\[9\.D\.PPCO\]/,/<\/tbody>/!d;/<\/tbody>/q' $CHU2 | egrep 'td class="(l|r) t data"'|sed -E 's/<td .*>(.*)<\/td>/\1/g'|paste - - -d'\|'|egrep "(Nombre total de consultations méd.*primo)|(File active)"|recode UTF8..html>$T2

        rm -f  res/ppco.txt


        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FILENAME~/T?1/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        FILENAME~/T?2/{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1)in valo){
            valo2[trim($1)]+=$2;
        }
        next;
    }

END{
print "{\"data\": [";
for (j=0;j<i;j++){
    s_1=valo[order[j]];
    if(order[j] in valo2){
        s_2=valo2[order[j]];
        s_3=s_1-s_2;
    }
else{
    s_2="\"\"";s_3="\"\""
    not_rel=1
}
if(s_2!=0&&!not_rel) {
    pc=100*(s_1-s_2)/s_2
}
            else{
                pc="\"\""
            }
            if(s_1-s_2<0){
                sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
            }
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0)print "[\""order[j]"\","valo[order[j]]","s_2","s_1-s_2","pc",\""sup"\"]";
            if(j<i-1)print","
            }
            print "]}"
        }' $T1 $T2> res/ppco.txt

        #**************LAMDA MCO**********************
        if [ $ANNEE1 -lt 2016 ]
        then
            ssconvert --export-type=Gnumeric_stf:stf_assistant -O 'separator=; charset=UTF-8 locale=fr_FR eol=windows' $AV1 fd://1 2>/dev/null| sed '/Montants hors AME/,/^Total/!d;/^Total/q'|tail -n+4|awk -F';' '$1!~/^Total/{gsub("\"","",$0);gsub(",",".",$2);gsub(",",".",$3);res=$3>0?$3:$2;sum+=res;printf("%s|%-\478.2f\n",$1,res);next}{printf("%s|%-\478.2f\n",$1,sum)}'|recode UTF8..html>$T1
        else
            ssconvert --export-type=Gnumeric_stf:stf_assistant -O 'separator=; charset=UTF-8 locale=fr_FR eol=windows' $AV1 fd://1 2>/dev/null| sed '/Montants hors AME/,/^Total/!d;/^Total/q'|tail -n+4|awk -F';' '{gsub("\"","",$0);gsub(",",".",$4);printf("%s|%-\478.2f\n",$1,$4)}'|recode UTF8..html>$T1
        fi

        if [ $MOISM1 -gt 0 ]
        then
            if [ $ANNEE1 -lt 2016 ]
            then
                ssconvert --export-type=Gnumeric_stf:stf_assistant -O 'separator=; charset=UTF-8 locale=fr_FR eol=windows' $AVM1 fd://1 2>/dev/null| sed '/Montants hors AME/,/^Total/!d;/^Total/q'|tail -n+4|awk -F';' '$1!~/^Total/{gsub("\"","",$0);gsub(",",".",$2);gsub(",",".",$3);res=$3>0?$3:$2;sum+=res;printf("%s|%-\478.2f\n",$1,res);next}{printf("%s|%-\478.2f\n",$1,sum)}'|recode UTF8..html>$T2    
            else
                ssconvert --export-type=Gnumeric_stf:stf_assistant -O 'separator=; charset=UTF-8 locale=fr_FR eol=windows' $AVM1 fd://1 2>/dev/null| sed '/Montants hors AME/,/^Total/!d;/^Total/q'|tail -n+4|awk -F';' '{gsub("\"","",$0);gsub(",",".",$4);printf("%s|%-\478.2f\n",$1,$4)}'|recode UTF8..html>$T2
            fi

        else
            rm -f $T2
            unset T2
        fi


        rm -f  res/lamda.txt

        awk -F'|' '
        function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
        function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
        function trim(s) { return rtrim(ltrim(s)); }
        FNR==NR{
        gsub(" ","",$2);
        gsub(",",".",$2);
        if(trim($1) in valo == 0)order[i++]=trim($1);
            valo[trim($1)]+=$2;
            next;
        }
        {
            gsub(" ","",$2);
            gsub(",",".",$2);
            if(trim($1)in valo){
                valo2[trim($1)]+=$2;
            }
            next;
        }

    END{
    print "{\"data\": [";
    for (j=0;j<i;j++){
        s_1=valo[order[j]];
        if(length(valo2)>0&&order[j] in valo2){
            s_2=valo2[order[j]];
            s_3=s_1-s_2;
        }
    else{
        s_2="\"\"";s_3="\"\""
        not_rel=1
    }
    if(s_2!=0&&!not_rel) {
        pc=100*(s_1-s_2)/s_2
    }
else{
    pc="\"\""
}
if(s_1-s_2<0){
    sup="<span class=\\\"glyphicon glyphicon-arrow-down\\\"></span>"
}
        else if (s_1-s_2>0){
            sup="<span class=\\\"glyphicon glyphicon-arrow-up\\\"></span>"
        }
    else{sup="<span class=\\\"glyphicon glyphicon-arrow-right\\\"></span>"}
        if(trim(length(order[j]))>0){
            print "[\""order[j]"\","sprintf("%.2f",valo[order[j]])
            if (length(valo2)>0){
                print","sprintf("%.2f",s_2)","sprintf("%.2f",s_1-s_2)","pc",\""sup"\"]"
            }
        else{
            print "]"
        }
    }
    if(j<i-1)print","
    }
    print "]}"
}' $T1 $T2  > res/lamda.txt


