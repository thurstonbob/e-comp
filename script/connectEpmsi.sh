#!/bin/bash
#cd $(dirname $0)
source "/var/www/html/ecmp/script/.proxy_conf"
DEST="https://epmsi.atih.sante.fr/cas/caslogin.do"
CAS_HOSTNAME=cas.atih.sante.fr
COOKIE_JAR=.cookieJar
HEADER_DUMP_DEST=.headers
rm -f $COOKIE_JAR
rm -f $HEADER_DUMP_DEST
rm -f rescon
USERNAME=$1
PASSWORD=$2

REP=`curl -x http://$login:$pwd@$host:$port --proxy-basic -s -k -L -c $COOKIE_JAR https://epmsi.atih.sante.fr/authenticate.do | egrep "name=(.lt|.execution)"`
CAS_ID=`echo "$REP"| grep name=.lt     | sed 's/.*value..//' | sed 's/\".*//'`
EXEC=`echo "$REP" | grep name=.execution | sed 's/.*value..//' | sed 's/\".*//'`
ENC_EX=`echo "$EXEC" | perl -p -e 's/([^A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg' | sed 's/%2E/./g' | sed 's/%0A//g'`
curl -x http://$login:$pwd@$host:$port --proxy-basic -s -k -d "username=$USERNAME" -d "password=$PASSWORD" -d "lt=$CAS_ID" -d "_eventId=submit" -d "submit=SE+CONNECTER" --data-urlencode "execution=$EXEC" -i -b $COOKIE_JAR -c $COOKIE_JAR https://$CAS_HOSTNAME/cas/login?service=$DEST -D $HEADER_DUMP_DEST>rescon
if grep -q "error " "rescon"; then
    #verifier comment est le message en cas de mauvaise connexion
    echo "mauvais identifiants"
    exit 1
else
    CURL_DEST=`grep Location $HEADER_DUMP_DEST | sed 's/Location: //'`
    C1=$(echo "$CURL_DEST"|cut -d? -f1)
    C2=$(echo "$CURL_DEST"|cut -d? -f2)
    Ticket=$(echo "$C2"|cut -d\& -f2|cut -d= -f2)
idProfil=`curl -x http://$login:$pwd@$host:$port --proxy-basic -G -s -k -L -b $COOKIE_JAR -c $COOKIE_JAR --data-urlencode "$C2" $C1 |grep -B1 "CHU DE NANCY"|grep name=.idProfil|sed 's/.*value..//'| sed 's/\".*//' `
curl -x http://$login:$pwd@$host:$port --proxy-basic  -s -k -b $COOKIE_JAR -c $COOKIE_JAR -d "ticket=$Ticket" -d "service=$DEST" -d "idProfil=$idProfil"  "https://selection-profil.atih.sante.fr/selection-profil/profil/selection" -o /dev/null
    
    curl -x http://$login:$pwd@$host:$port --proxy-basic -s -k -b $COOKIE_JAR -c $COOKIE_JAR https://$CAS_HOSTNAME/cas/login?service=$DEST -D $HEADER_DUMP_DEST
   CURL_DEST=`grep Location $HEADER_DUMP_DEST | sed 's/Location: //'`
   CURL_DEST=${CURL_DEST%$'\r'}
 curl -x http://$login:$pwd@$host:$port --proxy-basic -G --location-trusted -s -L  -k  -b $COOKIE_JAR -c $COOKIE_JAR  "$CURL_DEST" -o /dev/null
    curl -x http://$login:$pwd@$host:$port --proxy-basic -s -k  -L -b .cookieJar "https://epmsi.atih.sante.fr/jsp/epmsi/applis/applis.jsp" -o /dev/null
    curl -x http://$login:$pwd@$host:$port --proxy-basic -s   --location-trusted -b .cookieJar -c .cookieJar "https://epmsi.atih.sante.fr/appli_16.do?champPmsi=1&statut=1&applicationType=3"  |sed -nr '/hospitalInfo.do\?id/{s/.*>([0-9]+)<.*/\1/g;p}'
    curl -x http://$login:$pwd@$host:$port --proxy-basic -s -k  -L -b .cookieJar --referer "https://epmsi.atih.sante.fr/jsp/epmsi/applis/applisMat2a.jsp" "https://epmsi.atih.sante.fr/appli_16.do?champPmsi=1&statut=1&applicationType=3"  -o /dev/null

    exit 0
fi



