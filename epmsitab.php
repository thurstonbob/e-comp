<?php 
session_start();
IF(ISSET($_SESSION['id'])){
?>
<style>
        .bold {
        font-weight:bold;
        } 
</style>

<ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#gen" aria-controls="gen" role="tab" data-toggle="tab">G&eacute;n&eacute;ral</a></li>
    <li role="presentation" ><a href="#mco" aria-controls="mco" role="tab" data-toggle="tab">MCO</a></li>
    <li role="presentation"><a href="#externe" aria-controls="externe" role="tab" data-toggle="tab">Externe</a></li>
        <li role="presentation"><a href="#fichsup" aria-controls="fichsup" role="tab" data-toggle="tab">Fichsup</a></li>
        <li role="presentation"><a href="#lamda" aria-controls="lamda" role="tab" data-toggle="tab">LAMDA</a></li>


</ul>
<?
    $annee1=2018;
    $mois1=1;
    $annee2=2017;
    $mois2=1;
    if(isset($_POST['annee1'])){$annee1=$_POST['annee1'];}
    if(isset($_POST['annee2'])){$annee2=$_POST['annee2'];}
    if(isset($_POST['mois1'])){$mois1=$_POST['mois1'];}
    if(isset($_POST['mois2'])){$mois2=$_POST['mois2'];}
    passthru('script/epmsi_ycmp_v4.sh '.$mois1.' '.$annee1.'  '.$mois2.' '.$annee2.' '.$_SESSION['id'].' '.$_SESSION['pwd'].' 2>&1',$ret);
    if($ret==1)
    {echo "Pas de données pour la période sélectionnée";}
    else{

?>
        <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="gen">
<?php
        if(file_exists('res/exh.html'))
        {
            include('res/exh.html');

        }else
        {echo "Pas de r&eacute;sultat Exhaustivit&eacute;";}
        if(file_exists('res/gen_rsa.txt'))
        {
            echo'<h3>RSA transmis</h3><table id="rsa_tab" class="table display table-striped table-hover table-condensed" width="100%"><hr><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#rsa_tab').DataTable( {
                "ajax": 'res/gen_rsa.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat RSA";}
        if(file_exists('res/gen_ipdms.txt'))
        {
            echo'<h3>IPDMS</h3><table id="ipdms_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>'
?>
                <script type="text/javascript">
                $(document).ready( function () {
                    $('#ipdms_tab').DataTable( {
                    "ajax": 'res/gen_ipdms.txt',
                        "language": {
                        "decimal": ",",
                            "thousands": "."
        },
            "columns": [
                { },
                {
                    render: function (data, type, row) {
                        return row[1].toLocaleString();
        } 
        },
        {
            render: function (data, type, row) {
                return row[2].toLocaleString();
        } 
        },
        {
            render: function (data, type, row) {
                return row[3].toLocaleString();
        } 
        },
        {render: $.fn.dataTable.render.number('', ',',2,'','%')},
        { "type": "html" }
    ],							
    "columnDefs": [
        { className: "bold", "targets": [0] },
        { className: "text-right", "targets": [ 1,2,3,4 ] }
    ],
    createdRow: function (row, data, index) {
        if (data[0] == "IPDMS") {
            res=data[3]>0?'danger':'success';
            $(row).addClass(res);
        }
        },
            dom: 'Bfrtip',
            buttons: [
        {
            extend: 'excel',
                exportOptions: {
                format: {
                body: function(data, row, column, node) {
                    data = $('<p>' + data + '</p>').text();
                    data=column!=0?data.replace(' ',''):data;
                    return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat IPDMS";}
        if(file_exists('res/exhb.txt'))
        {
            echo'<h3>Extr&ecirc;me haut/bas</h3><table id="exhb_tab" class="table table-striped table-hover table-condensed width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#exhb_tab').DataTable( {
                "ajax": 'res/exhb.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat Extreme Haut/Bas";}

        if(file_exists('res/ivg.txt'))
        {
            echo'<h3>IVG</h3><table id="ivg_tab" class="table table-striped table-hover table-condensed width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#ivg_tab').DataTable( {
                "ajax": 'res/ivg.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat IVG";}
        if(file_exists('res/ft.txt'))
        {
            echo'<h3>Mol&eacute;cules on&eacute;reuses</h3><table id="mo_tab" class="table table-striped table-hover table-condensed width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#mo_tab').DataTable( {
                "ajax": 'res/mo.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat Mol&eacute;cules on&eacute;reuses";}
        if(file_exists('res/dmi.txt'))
        {
            echo'<h3>DMI</h3><table id="dmi_tab" class="table table-striped table-hover table-condensed width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#dmi_tab').DataTable( {
                "ajax": 'res/dmi.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat DMI";}

?>
                </div>
                <div role="tabpanel" class="tab-pane" id="mco">
<?php


        if(file_exists('res/hosp_t2a.txt'))
        {
            echo'<h3>T2A</h3><table id="t2a_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th></tr></thead></table>';
            echo'<h3>Coefficient prudentiel</h3><table id="cp_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th></tr></thead></table>';
            echo'<h3>Assurance Maladie</h3><table id="am_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th></tr></thead></table>';	
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#t2a_tab').DataTable( {
                "ajax": 'res/hosp_t2a.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ',',',0,'',' &euro;')},
            {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
            {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        createdRow: function (row, data, index) {
            if (data[0] == "Total") {
                res=data[3]<0?'danger':'success';
                $(row).addClass(res);
        }
        },
            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'excel',
                exportOptions: {
                format: {
                body: function(data, row, column, node) {
                    data = $('<p>' + data + '</p>').text();
                    data=column!=0?data.replace(/ |€/g,''):data;
                    return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });
        $('#cp_tab').DataTable( {
        "ajax": 'res/hosp_cp.txt',
            "language": {
            "decimal": ",",
                "thousands": "."
        },
            "columns": [
        { },
        {render: $.fn.dataTable.render.number(' ',',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number('', ',',2,'','%')},
        { "type": "html" }
    ],							
    "columnDefs": [
        { className: "bold", "targets": [0] },
        { className: "text-right", "targets": [ 1,2,3,4 ] }
    ],
    createdRow: function (row, data, index) {
        if (data[0] == "Total") {
            res=data[3]<0?'danger':'success';
            $(row).addClass(res);
        }
        },
            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'excel',
                exportOptions: {
                format: {
                body: function(data, row, column, node) {
                    data = $('<p>' + data + '</p>').text();
                    data=column!=0?data.replace(/ |€/g,''):data;
                    return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });
        $('#am_tab').DataTable( {
        "ajax": 'res/hosp_am.txt',
            "language": {
            "decimal": ",",
                "thousands": "."
        },
            "columns": [
        { },
        {render: $.fn.dataTable.render.number(' ',',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number('', ',',2,'','%')},
        { "type": "html" }
    ],							
    "columnDefs": [
        { className: "bold", "targets": [0] },
        { className: "text-right", "targets": [ 1,2,3,4 ] }
    ],
    createdRow: function (row, data, index) {
        if (data[0] == "Total") {
            res=data[3]<0?'danger':'success';
            $(row).addClass(res);
        }
        },
            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'excel',
                exportOptions: {
                format: {
                body: function(data, row, column, node) {
                    data = $('<p>' + data + '</p>').text();
                    data=column!=0?data.replace(/ |€/g,''):data;
                    return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });

        });
        </script>
<?
        }
        else
        {echo "Pas de r&eacute;sultat Hospitalisation MCO";}
?>
                </div>
                <div role="tabpanel" class="tab-pane" id="externe">
<?php
        if(file_exists('res/ext_t2a.txt'))
        {
            echo'<h3>T2A</h3><table id="et2a_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2	.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th></tr></thead><tbody></table>';
            echo'<h3>Assurance Maladie</h3><table id="eam_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2	.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th></tr></thead><tbody></table>';
            echo'<h3>T2A Urgences</h3><table id="eurg_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2	.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th></tr></thead><tbody></table>';
            echo'<h3>Assurance Maladie Urgences</h3><table id="eamurg_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2	.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th></tr></thead><tbody></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#et2a_tab').DataTable( {
                "ajax": 'res/ext_t2a.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ',',',0,'',' &euro;')},
            {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
            {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        createdRow: function (row, data, index) {
            if (data[0] == "Total") {
                res=data[3]<0?'danger':'success';
                $(row).addClass(res);
        }
        },
            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'excel',
                exportOptions: {
                format: {
                body: function(data, row, column, node) {
                    data = $('<p>' + data + '</p>').text();
                    data=column!=0?data.replace(/ |€/g,''):data;
                    return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });
        $('#eam_tab').DataTable( {
        "ajax": 'res/ext_am.txt',
            "language": {
            "decimal": ",",
                "thousands": "."
        },
            "columns": [
        { },
        {render: $.fn.dataTable.render.number(' ',',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number('', ',',2,'','%')},
        { "type": "html" }
    ],							
    "columnDefs": [
        { className: "bold", "targets": [0] },
        { className: "text-right", "targets": [ 1,2,3,4 ] }
    ],
    createdRow: function (row, data, index) {
        if (data[0] == "Total") {
            res=data[3]<0?'danger':'success';
            $(row).addClass(res);
        }
        },
            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'excel',
                exportOptions: {
                format: {
                body: function(data, row, column, node) {
                    data = $('<p>' + data + '</p>').text();
                    data=column!=0?data.replace(/ |€/g,''):data;
                    return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });
        $('#eurg_tab').DataTable( {
        "ajax": 'res/ext_t2a_u.txt',
            "language": {
            "decimal": ",",
                "thousands": "."
        },
            "columns": [
        { },
        {render: $.fn.dataTable.render.number(' ',',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number('', ',',2,'','%')},
        { "type": "html" }
    ],							
    "columnDefs": [
        { className: "bold", "targets": [0] },
        { className: "text-right", "targets": [ 1,2,3,4 ] }
    ],
    createdRow: function (row, data, index) {
        if (data[0] == "Total") {
            res=data[3]<0?'danger':'success';
            $(row).addClass(res);
        }
        },
            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'excel',
                exportOptions: {
                format: {
                body: function(data, row, column, node) {
                    data = $('<p>' + data + '</p>').text();
                    data=column!=0?data.replace(/ |€/g,''):data;
                    return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });
        $('#eamurg_tab').DataTable( {
        "ajax": 'res/ext_am_u.txt',
            "language": {
            "decimal": ",",
                "thousands": "."
        },
            "columns": [
        { },
        {render: $.fn.dataTable.render.number(' ',',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number(' ', ',',0,'',' &euro;')},
        {render: $.fn.dataTable.render.number('', ',',2,'','%')},
        { "type": "html" }
    ],							
    "columnDefs": [
        { className: "bold", "targets": [0] },
        { className: "text-right", "targets": [ 1,2,3,4 ] }
    ],
    createdRow: function (row, data, index) {
        if (data[0] == "Total") {
            res=data[3]<0?'danger':'success';
            $(row).addClass(res);
        }
        },
            dom: 'Bfrtip',

            buttons: [
        {
            extend: 'excel',
                exportOptions: {
                format: {
                body: function(data, row, column, node) {
                    data = $('<p>' + data + '</p>').text();
                    data=column!=0?data.replace(/ |€/g,''):data;
                    return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });

        });
        </script>
<?

        }
        else
        {echo "Pas de r&eacute;sultat Externe";}
        if(file_exists('res/ft.txt'))
        {
            echo'<h3>Forfaits techniques</h3><table id="ft_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead><tbody></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#ft_tab').DataTable( {
                "ajax": 'res/ft.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat Forfaits techniques";}
        if(file_exists('res/dmext.txt'))
        {
            echo'<h3>DM en externe</h3><table id="dmext_tab" class="table display table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead><tbody></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#dmext_tab').DataTable( {
                "ajax": 'res/dmext.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat DM en externe";}
?>
                </div>
                <div role="tabpanel" class="tab-pane" id="fichsup">
<?if(file_exists('res/ahcbhn.txt'))
        {
            echo'<h3>AHC/BHN liste RIHN</h3><table id="ahcbhn_tab" class="table display table-striped table-hover table-condensed" width="100%"><hr><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#ahcbhn_tab').DataTable( {
                "ajax": 'res/ahcbhn.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
        "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat AHC/BHN liste RIHN";}
        if(file_exists('res/ahcbhn2.txt'))
        {
            echo'<h3>AHC/BHN liste compl&eacute;mentaire</h3><table id="ahcbhn2_tab" class="table display table-striped table-hover table-condensed" width="100%"><hr><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#ahcbhn2_tab').DataTable( {
                "ajax": 'res/ahcbhn2.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat AHC/BHN liste compl&eacute;mentaire";}
        if(file_exists('res/ahcbhn3.txt'))
        {
            echo'<h3>AHC/BHN Total</h3><table id="ahcbhn3_tab" class="table display table-striped table-hover table-condensed" width="100%"><hr><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#ahcbhn3_tab').DataTable( {
                "ajax": 'res/ahcbhn3.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat AHC/BHN Total";}
        if(file_exists('res/smur.txt'))
        {
            echo'<h3>SMUR</h3><table id="smur_tab" class="table display table-striped table-hover table-condensed" width="100%"><hr><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#smur_tab').DataTable( {
                "ajax": 'res/smur.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat SMUR";}
        if(file_exists('res/ces.txt'))
        {
            echo'<h3>CES</h3><table id="ces_tab" class="table display table-striped table-hover table-condensed" width="100%"><hr><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#ces_tab').DataTable( {
                "ajax": 'res/ces.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat CES";}

        if(file_exists('res/ppco.txt'))
        {
            echo'<h3>PPCO</h3><table id="ppco_tab" class="table display table-striped table-hover table-condensed" width="100%"><hr><thead><tr><th></th><th style="text-align:right">'.$mois1.' / '.$annee1.'</th><th style="text-align:right">'.$mois2.' / '.$annee2.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th></th></tr></thead></table>';
?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#ppco_tab').DataTable( {
                "ajax": 'res/ppco.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number(' ', ',') },
            {render: $.fn.dataTable.render.number('', ',',2,'','%')},
            { "type": "html" }
        ],							
        "columnDefs": [
            { className: "bold", "targets": [0] },
            { className: "text-right", "targets": [ 1,2,3,4 ] }
        ],
        dom: 'Bfrtip',

        buttons: [
            {
                extend: 'excel',
                    exportOptions: {
                    format: {
                    body: function(data, row, column, node) {
                        data = $('<p>' + data + '</p>').text();
                        data=column!=0?data.replace(',','.'):data;
                        return column!=0?data.replace(/ /g, ','):data;
        }
        }
        }
        }
    ]
        });
        });
        </script>
<?
        }else
        {echo "Pas de r&eacute;sultat PPCO";}
?>
                </div>
                <div role="tabpanel" class="tab-pane" id="lamda">
<?php
        if(file_exists('res/lamda.txt'))
        {
            $string = file_get_contents("res/lamda.txt");
            $json_a = json_decode($string, true);
            $elementCount  = count($json_a['data'][0]);
            echo'<h3>LAMDA</h3><table id="lamda_tab" class="table table-striped table-hover table-condensed" width="100%"><thead><tr><th></th><th style=\"text-align:right\">'.$mois1.' / '.$annee1.'</th>';
            if($elementCount>2)echo'<th style="text-align:right">'.($mois1-1).' / '.$annee1.'</th><th style="text-align:right">Diff.</th><th style="text-align:right">%</th><th style="text-align:right"></th>';
            echo'</tr></thead></table>';

?>
            <script type="text/javascript">
            $(document).ready( function () {
                $('#lamda_tab').DataTable( {
                "ajax": 'res/lamda.txt',
                    "language": {
                    "decimal": ",",
                        "thousands": "."
        },
            "columns": [
            { },
            {render: $.fn.dataTable.render.number(' ',',',2,'',' &euro;')}
<? if($elementCount>2)echo",
{render: $.fn.dataTable.render.number(' ', ',',2,'',' &euro;')},
    {render: $.fn.dataTable.render.number(' ', ',',2,'',' &euro;')},
    {render: $.fn.dataTable.render.number('', ',',2,'','%')},
    { 'type': 'html' }";?>
],
"columnDefs": [
    { className: "bold", "targets": [0] },
<? if($elementCount>2){
echo'{ className: "text-right", "targets": [ 1,2,3,4 ] }';
}
else{
    echo'{ className: "text-right", "targets": [1] }';
}

?>
],
    dom: 'Bfrtip',
    buttons: [
{
    extend: 'excel',
        exportOptions: {
        format: {
        body: function(data, row, column, node) {
            data = $('<p>' + data + '</p>').text();
            data=column!=0?data.replace(/ |€/g,''):data;
            return column!=0?data.replace(',', '.'):data;
        }
        }
        }
        }
    ]
        });

        } );
        </script>
<?
        }
        else
        {echo "Pas de r&eacute;sultat LAMDA";}
?>
                </div>
        </div>
<?
    }
}else{
    echo "<script language=\"javascript\">alert(\"Please login\");document.location.href='login.php';</script>"; 
}
?>
