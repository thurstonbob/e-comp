<?php
IF(ISSET($_POST['username'])){
    $login = $_POST['username'];
    $password = $_POST['password'];
    $msg=system('script/connectEpmsi.sh '.$login.' '.$password.' > /dev/null 2>&1', $retval);
    if($retval==0){
        session_start();
        $_SESSION['id'] = $login;
        $_SESSION['pwd'] = $password;
?>
        <!DOCTYPE html>
        <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                        <meta charset="utf-8">
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title> 
Comparateur ePMSI
                        </title>
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<!--                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jq-2.2.4/dt-1.10.13/b-1.2.4/b-html5-1.2.4/b-print-1.2.4/datatables.min.css"/>-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/b-1.5.0/b-html5-1.5.0/datatables.min.css"/>

                        <link rel="icon" type="image/png" href="favicon.ico" />
                        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
                        <link href='https://fonts.googleapis.com/css?family=Maven+Pro' rel='stylesheet' type='text/css'>
                        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

                        <link rel="stylesheet" href="style/ecmp.css">

                        <!-- Latest compiled and minified CSS -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.16/b-1.5.0/b-html5-1.5.0/datatables.min.js"></script>
<script type="text/javascript" src="script/ecmp.js"></script>


</head>
                <body>
                        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                                <div class="container-fluid">
                                        <!-- LOGO -->
                                        <div class="navbar-header">
                                                <a class="navbar-brand logo" href="index.php" title="Accueil">
                                                        <img src="/tbdim/images/img/logo6.png" alt="Accueil" class="logo-img">
                                                </a>
                                                <!-- //LOGO -->

                                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span> 
                                                </button>

                                        </div>

                                        <nav class="collapse navbar-collapse"  id="myNavbar">
                                                <ul class="nav navbar-nav">
                                                        <li>
                                                                <a data-page="epmsi" href="#">Comparateur</a>

                                                        </li>

                                                </ul>    
                                                <ul class="nav navbar-nav navbar-right">
                                                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> <?=$_SESSION['id'];?> </a></li>
                                                        <li><a href="logout.php?destroy"><span class="glyphicon glyphicon-log-in"></span> Quitter</a></li>
                                                </ul>
                                        </nav>
                                </div>
                        </nav>
                        <div class="main">
                <div class="jumbotron" id="pres">
                        <div class="container">
                                <h1>Comparateur ePMSI</h1>
                                <p>Cette application permet de r&eacute;cup&eacute;rer le tableau de valorisation d'une p&eacute;riode sur ePMSI et fait la comparaison avec l'ann&eacute;e N-1.</p>					
                                <p class="lead" style="color:#365a7d">Choisir les p&eacute;riodes ci-dessous</p>
                        </div>
                </div>
                <div class="container">
                        <div class="row">				
                                <div class="col-md-12" style="border-radius:10px;background:#dbe6f0;padding:7px 0 7px 0">
                                        <form method="post" action="epmsitab.php" role="form" class="form-inline" name="epmsi" id="epmsi" ajax="true">
                                                <div class="col-md-2" style="border-right: solid #ffffff;color:#365a7d;padding-left:25px"> 
                                                        <select class="selectpicker" data-style="btn-info" data-width="auto" name="typ" id="typ">
                                                                <option value="aam1" selected>Ann&eacute;e N/N-1</option>
                                                                <option value="mm0">Mois M/Test</option>
                                                                <option value="free">Libre</option>
                                                        </select>                                                        
                                                </div>				
                                                <div class="form-group col-md-4">

                                                        <label for="mois1" class="control-label">Mois 1 :</label>
                                                        <select id="mois1" class="selectpicker" data-width="auto" name="mois1">
                                                                <option <? if(!isset($_POST['mois1'])||(isset($_POST['mois1'])&& $_POST['mois1']=="1")){echo "selected";}  ?> value="1" > Janvier </option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="2"){echo "selected";}?> value="2">Février</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="3"){echo "selected";}?> value="3">Mars</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="4"){echo "selected";}?> value="4">Avril</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="5"){echo "selected";}?> value="5">Mai</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="6"){echo "selected";}?> value="6">Juin</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="7"){echo "selected";}?> value="7">Juillet</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="8"){echo "selected";}?> value="8">Août</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="9"){echo "selected";}?> value="9">Septembre</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="10"){echo "selected";}?> value="10">Octobre</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="11"){echo "selected";}?> value="11">Novembre</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="12"){echo "selected";}?> value="12">Décembre</option>
                                                                <option <? if(isset($_POST['mois1'])&& $_POST['mois1']=="0"){echo "selected";}?> value="0">Test</option>
                                                        </select>
                                                        <label for="annee1">Année 1 :</label>
                                                        <select id="annee1" class="selectpicker"  data-width="auto" name="annee1">
                                                                <option <? if(!isset($_POST['annee1'])||(isset($_POST['annee1'])&& $_POST['annee1']=="2018")){echo "selected";}  ?> value="2018"> 2018</option>
                                                                <option <? if(isset($_POST['annee1'])&& $_POST['annee1']=="2017"){echo "selected";}?> value="2017">2017</option>
                                                                <option <? if(isset($_POST['annee1'])&& $_POST['annee1']=="2016"){echo "selected";}?> value="2016">2016</option>
                                                        </select>
                                                </div>
                                                <div class="col-md-1">
                                                        <button class="btn btn-outline-info" id="inv"><span class="glyphicon glyphicon-transfer"></span></button>
                                                </div>
                                                <div class="form-group col-md-4" name="an2fg" id="an2fg">

                                                        <label id="m2lab" for="mois2" class="control-label">Mois 2 :</label>
                                                        <select id="mois2" class="selectpicker" data-width="auto" name="mois2" disabled> 
                                                                <option <? if(!isset($_POST['mois2'])||(isset($_POST['mois2'])&& $_POST['mois2']=="1")){echo "selected";}  ?> value="1" > Janvier </option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="2"){echo "selected";}?> value="2">Février</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="3"){echo "selected";}?> value="3">Mars</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="4"){echo "selected";}?> value="4">Avril</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="5"){echo "selected";}?> value="5">Mai</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="6"){echo "selected";}?> value="6">Juin</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="7"){echo "selected";}?> value="7">Juillet</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="8"){echo "selected";}?> value="8">Août</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="9"){echo "selected";}?> value="9">Septembre</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="10"){echo "selected";}?> value="10">Octobre</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="11"){echo "selected";}?> value="11">Novembre</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="12"){echo "selected";}?> value="12">Décembre</option>
                                                                <option <? if(isset($_POST['mois2'])&& $_POST['mois2']=="0"){echo "selected";}?> value="0">Test</option>
                                                        </select>
                                                        <label id="a2lab" for="annee2">Année 2 :</label>
                                                        <select id="annee2" class="selectpicker"  data-width="auto" name="annee2" disabled>
                                                                <option <? if(isset($_POST['annee2'])&& $_POST['annee2']=="2018"){echo "selected";}  ?> value="2018"> 2018 </option>
                                                                <option <? if(!isset($_POST['annee2'])||(isset($_POST['annee2'])&& $_POST['annee2']=="2017")){echo "selected";}?> value="2017">2017</option>
                                                                <option <? if(isset($_POST['annee2'])&& $_POST['annee2']=="2016"){echo "selected";}?> value="2016">2016</option>
                                                        </select>
                                                </div>
                                                <div class="col-md-1" style="color:#365a7d;padding-right:5px"> 
                                                        <button type="submit" id="myButton" data-loading-text="<i class='fa fa-spinner fa-spin'></i>&nbsp;Chargement..." class="btn btn-primary pull-right" autocomplete="off" style="padding:6px 6px;">
                                                                Comparer
                                                        </button>
                                                </div>
                                        </form>

                                </div><!--colmd12-->

                        </div><!--row-->
                        <br>
                        <div class="row">

                                <div class="col-md-10 col-md-offset-1" id="result">

                                </div>

                        </div>


                </div><!--container-->

                        </div>
                </body>
        </html>
<?php 
    }
    else{
    echo "<script language=\"javascript\">alert(\"Erreur dans l'identification ePMSI\");document.location.href='login.php';</script>"; 
    }

}else{
    echo "<script language=\"javascript\">alert(\"Merci de vous connecter\");document.location.href='login.php';</script>"; 
}
?>
