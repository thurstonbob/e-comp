<!DOCTYPE HTML>
<html>
        <head>
                <title>Comparateur ePMSI</title>


                <!-- JQUERY -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
                <!-- bootstrap-3.3.7 -->
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

                <link href="style/style.css" rel="stylesheet" type="text/css">

        </head>
        <body>
                <div class="container">
                        <div class="row" align="center">
                                <h1>Comparateur ePMSI</h1>
                        </div>
                </div>
                <div class="wrapper">
                        <form class="form-signin" action="index.php" method="post"> 
                                <h2 class="form-signin-heading">Identifiants ePMSI</h2>
                                <input type="text" class="form-control" name="username" placeholder="Nom d'utilisateur" required="" autofocus="" /><br>
                                <input type="password" class="form-control" name="password" placeholder="Mot de passe" required=""/>
                                <br>

                                <button class="btn btn-lg btn-primary btn-block" type="submit">Valider</button>   
                        </form>
                </div>
        </body>
</html>
